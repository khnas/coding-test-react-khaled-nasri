import React, { Component, Fragment } from "react";
import Helmet from "react-helmet";
import { Link } from "react-router-dom";
import Icon from "@material-ui/core/Icon";

import { title } from "utils";
import { Article } from "components";
import { userService } from "services";

class UserPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      id: null,
      user: {},
      userLoaded: false,
    };
  }

  componentDidMount() {
    userService.list().then((result) => {
      this.setState({ list: result, id: result[0].id });
      this.updateUserInfo(this.state.id);
    });
  }

  async updateUserInfo(id) {
    let promise = userService.getUser(id);

    let result = await promise;
    this.setState({ user: result, userLoaded: true });
    console.log(this.state.user);
  }

  handleChange = (event) => {
    this.setState({ id: event.target.value });
    console.log(event.target.value);
    this.updateUserInfo(event.target.value);
  };

  render() {
    return (
      <Fragment>
        <Helmet>{title("Page secondaire")}</Helmet>

        <div className="user-page content-wrap">
          <Link to="/" className="nav-arrow">
            <Icon style={{ transform: "rotate(180deg)", fontSize: 70 }}>
              arrow_right_alt
            </Icon>
          </Link>

          <div className="users-select">
            <h1>
              <select
                className="users-select-select"
                onChange={this.handleChange}
                value={this.state.id}
              >
                {this.state.list.map((value) => {
                  return (
                    <option className="users-select-option" value={value.id}>
                      Utilisateur # {value.id}
                    </option>
                  );
                })}
              </select>
            </h1>
          </div>

          <div className="infos-block">
            <h2 className="infos-content">
              Name: <span>{this.state.user.name}</span>
            </h2>
            <h2 className="infos-content">
              Occupation: <span>{this.state.user.occupation}</span>
            </h2>
            <h2 className="infos-content">
              Date de naissance: <span>{this.state.user.birthdate}</span>
            </h2>
          </div>

          <div className="articles-list">
            {this.state.userLoaded ? (
              this.state.user.articles.map((item) => {
                return <Article name={item.name} content={item.content} />;
              })
            ) : (
              <h1>no articles to display</h1>
            )}
          </div>
        </div>
      </Fragment>
    );
  }
}

export default UserPage;
