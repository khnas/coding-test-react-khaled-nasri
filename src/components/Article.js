import React, { Component } from "react";

class Article extends Component {
  render() {
    return (
      <div className="article-block">
        <h2 className="article-name">{this.props.name}</h2>
        <p className="article-content">{this.props.content}</p>
      </div>
    );
  }
}

export default Article;
